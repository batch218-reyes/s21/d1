console.log("Hello, world!")


//List of studentIDs of all graduationg student of the class.
console.log("Variable cs Array");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1923";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

console.log(studentNumberA);
console.log(studentNumberB);
console.log(studentNumberC);
console.log(studentNumberD);
console.log(studentNumberE);

						//0         //1           //2          //3           //4    -index
let studentNumbers = ["2020-1923", "2020-1923", "2020-1925", "2020-1926",  "2020-1927"]
						// this are the elements 
console.log(studentNumbers);
console.log("-----------------------------------------------------------------------------");
// [SECTION] Arrays
/*
        - Arrays are used to store multiple related values in a single variable.
        - They are declared using square brackets ([]) also known as "Array Literals"
        - Arrays it also provides access to a number of functions/methods that help in manipulation array.
            - Methods are used to manipulate information stored within the same object.
        - Array are also objects which is another data type.
        - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
        - Syntax:
            let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
    */

 console.log("Examples of Array: ")

 		// common examples of arrays
 		let grades = [98.5, 94.3, 89.2, 90]; //arrays cound store nnumberic values 
 		let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "fujitsu"];
 		// arrays could store string values 

 console.log(grades);
 console.log(computerBrands);

 // possible use of an array but it is not recomended 
 // it is not recomended for it is hard to define what is the purpose of the arrary or what are data insde array and where the programmer or co-pgrammer could use it.
 let mixedArr = ["John", "Doe", 12, false, null, undefined,{}];
 console.log(mixedArr);


 // Altermative way creating an array 
 let myTasks = [
 	"drink html",
 	"eat javascript",
 	"inhale css",
 	"Bake mongoDBs",
 ];
 console.log(myTasks);

 console.log("-----------------------------------------------------------------------------");


// creating an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);
console.log ("-----------end of a line --------")

// [SECTION] .leghth property
// ".length" property allows us to get and set the total number of items in an array
console.log ("Using . length property for array size: ");
console.log("length. size of myTask array: " + myTasks.length); // the output should be 4
console.log("length. size of cities array: " + cities.length); // the output should be 3

console.log("----------------------------")

console.log ("Using . length property for string size: ")

let fullName = "Ren Gabriel Almario reyes"
console.log ("length/size of fullName string Variable: " + fullName.length) // spaces are added 

console.log("-------------------")

/* let myTasks = [
 	"drink html",
 	"eat javascript",
 	"inhale css",
 	"Bake mongoDBs",
 ];
 console.log(myTasks);*/

console.log ("removing the last element from an array");
myTasks.length = myTasks.length - 1;
//4      		//4 					// the last element will be removed

console.log(myTasks.length);
console.log(myTasks);

// to delete a specific item in an array  we can also employ array methods. we have shown the logic or algorithm of "pop method";

//let cities = [city1, city2, city3];
cities.length--;
console.log(cities);


// we cant do the same on string 
// let fullName = "Ren Gabriel Almario reyes"
fullName.length = fullName.length -1;
console.log(fullName.length);
console.log(fullName);

console.log("----------------------")

// we can also add the length of an array.
console.log("Add an element to an array");
let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);

theBeatles[theBeatles.length] = "Cardo"; // to add another array.
console.log(theBeatles);

console.log ("-----------------------------------")

// [SECTION] Reading from Arrays
    /*
        - Accessing array elements is one of the common task that we do with an array.
        - This can be done through the use of array indexes.
        - Each element in an array is associated with it's own index number.
        - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
        - Array indexes it is actually refer to a memory address/location

        Array Address: 0x7ffe942bad0
        Array[0] = 0x7ffe942bad0
        Array[1] = 0x7ffe942bad4
        Array[2] = 0x7ffe942bad8

        - Syntax
            arrayName[index];
    */

  //let grades = [98.5, 94.3, 89.2, 90];
  console.log(grades[0]); //index number of the first element is zero.



  						//1      //2       //3      //4     //5 	- regular/usual count 
						//0     //1       //2      //3     //4      - indexes	
  //let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "fujitsu"];
console.log(computerBrands[3])

// Accessing an array element that does not exist will return undefined
console.log(grades[20]); // will retunr undeifined

let lakesLegends = ["Kobe", "shaq", "Lebron", "Magic", "Kareem","westbrook"];

console.log(lakesLegends[1] + " & " + lakesLegends[3]);

console.log ("----------------------------");
console.log("Reassigning an element from an array");
	// You can also reasign array values using indeces
	console.log("Array bofre Reassigning");
	console.log(lakesLegends);

	lakesLegends[2] = "Gasol";
	console.log("Array after Reassignment: ")
	console.log(lakesLegends);

console.log("----------------");

console.log("Access the last element of an array");
				
					//1         2         3        4  		5
					//0         1         2        3        4 - indeces
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex =bullsLegends.length -1; // length - 1 is equivalent to our last element
console.log(bullsLegends[lastElementIndex]); 

console.log("------------------------------")
console.log("Adding a new items into an array using indeces");
	// adding items into an array
	// we can add items in array using indeces 

const newArr =[];
console.log (newArr[0]); //undefined

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]); // undefined
newArr[1] = "Tifa Lockhart";
console.log(newArr);

console.log("------------------------------");


console.log ("add element in the end of an arrays using '.length' property");

// [Cloud Strife, "Tifa Lockhart"]
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

console.log("--------------------")
console.log("Display the content of an array 1 by 1");
// [Cloud Strife, "Tifa Lockhart" , "Barret wallace"]


// Exaple of accessing arrays elements 1 by 1 with console. log only 
// console.log(newArr[0]);
// console.log(newArr[1]);
// console.log(newArr[2]);

										//2
// [Cloud Strife, "Tifa Lockhart" , "Barret wallace"]
// initialization / declaration (let i=0)
// condition ( i < newArr.length)
// change of value(i++);
for(let index=0; index < newArr.length; index++ ){
	console.log(newArr[index]);

}
console.log("---------------------------");


    console.log("Filtering an array using loop and conditional statements: ");
    let numArr2 = [5, 12, 30, 46, 40, 52];

    // This code is to check which number is divisible by 5
    for (let index = 0; index < numArr2.length; index++){
        if(numArr2[index] % 5 == 0){
            console.log(numArr2[index] + " is divisible by 5.");
        }
        else {
            console.log(numArr2[index] + " is not divisible by 5.")
        }
    }


    // [SECTION] Multidimentional Arrays

/*
   -Multidimensional arrays are useful for storing complex data structures.
   - A practical application of this is to help visualize/create real world objects.
   - This is frequently used to store data for mathematic computations, image processing, and record management.
   - Array within an Array
*/


// Create chessboard
let chessBoard = [
   ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
   ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
   ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
   ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
   ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
   ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
   ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
   ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];

// let chessboard = [array, array, array, array, array, array, array, array];

console.table(chessBoard);

// access an element of a multidimensinal arrrays
// syntas: multiArr[outerarr][innerArray]
					//col 	//row
 
console.log(chessBoard[3][4]);   // e4

console.log ("pawn moves to; " + chessBoard[2][5]); //f3

